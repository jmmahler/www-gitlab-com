---
layout: markdown_page
title: "Objectives and Key Results (OKRs)"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## What are OKRs?

OKRs are our quarterly goals to execute our [strategy](https://about.gitlab.com/strategy/). To make sure our goals are clearly defined and aligned throughout the organization. For more information see [Wikipedia](https://en.wikipedia.org/wiki/OKR) and [Google Drive](https://docs.google.com/presentation/d/1ZrI2bP-XKEWDWsT-FLq5piuIwl84w9cYH1tE3X5oSUY/edit) (GitLab internal). The OKRs are our quarterly goals.

## Format

Before the quarter:

`Owner: Objective as a sentence. Key result, key result, key result.`

During and after the quarter:

`Owner: Key Result as a sentence. Key result, key result, key result. => Outcome, outcome, outcome.`

- Each objective has between 1 and 3 key results.
- Each key result has an outcome.
- Owner is the title of role that will own the result.
- We use four spaces to indent instead of tabs.
- The key result can link to an issue.
- The outcome can link to real time data about the current state.
- The three CEO objectives are level 3 headers to provide visual separation.

## Levels

We only list objectives prefaced with your role title.
We do OKRs up to the team or director level, we don't do [individual OKRs](https://hrblog.spotify.com/2016/08/15/our-beliefs/).
Part of the individual performance review is the answer to: how much did this person contribute to the team objectives?
We have no more than [five layers in our team structure](https://about.gitlab.com/team/structure/).
Because we go no further than the manager level we end up with a maximum 4 layers of indentation on this page.
The match of one "nested" key result with the "parent" key result doesn't have to be perfect.
Every owner should have at most 3 objectives. To make counting easier always mention the owner with a trailing colon, like `Owner:`.
The advantage of this format is that the OKRs of the whole company will fit on three pages, making it much easier to have an overview.

## Updating

The key results are updated continually throughout the quarter when needed.
Everyone is welcome to a suggestion to improve them.
To update: make a merge request and assign it to the CEO.
If you're a [team member](https://about.gitlab.com/team/) or in the [core team](https://about.gitlab.com/core-team/) please post a link to the MR in the #okrs channel and at-mention the CEO.

At the top of the OKRs is a link to the state of the OKRs at the start of the quarter so people can see a diff.

Timeline of how we draft the OKRs:

1. CEO pushes top goals to this page: 5 weeks before the start of the quarter
1. Executive team pushes updates to this page: 4 weeks before the start of the quarter
1. Executive team 90 minute planning meeting: 3 weeks before the start of the quarter
1. Discuss with the board and the teams: 2 weeks before the start of the quarter
1. Executive team 'how to achieve' presentations: 1 week before the start of the quarter
1. Add Key Results to top of 1:1 agenda's: before the start of the quarter
1. Present OKRs at a functional group update: first week of the quarter
1. Present 'how to achieve' at a functional group update: during first three weeks of the quarter
1. Review previous quarter and next during board meeting: after the start of the quarter

## Scoring

It's important to score OKRs after the quarter ends to make sure we celebrate what went well, and learn from what didn't in order to set more effective goals and/or execute better next quarter.

1. Move the current OKRs on this page to an archive page _e.g._ [2017 Q3 OKRs](/okrs/2017-q3/)
1. Add in-line comments for each key result briefly summarizing how much was achieved _e.g._
  * "=> Done"
  * "=> 30% complete"
1. Add a section to the archived page entitled "Retrospective"
1. OKR owners should add a subsection for their role outlining...
  * GOOD
  * BAD
  * TRY
1. Promote the draft OKRs on this page to be the current OKRs

## Critical acclaim

Spontaneous chat messages from team members after introducing this format:

> As the worlds biggest OKR critic, This is such a step in the right direction :heart: 10 million thumbs up

> I like it too, especially the fact that it is in one page, and that it stops at the team level.

> I like: stopping at the team level, clear reporting structure that isn't weekly, limiting KRs to 9 per team vs 3 per team and 3 per each IC.

> I've been working on a satirical blog post called called "HOT NEW MANAGEMENT TREND ALERT: RJGs: Really Just Goals" and this is basically that. :wink: Most of these are currently just KPIs but I won't say that too loudly :wink: It also embodies my point from that OKR hit piece: "As team lead, it’s your job to know your team, to keep them accountable to you, and themselves, and to be accountable for your department to the greater company. Other departments shouldn’t care about how you measure internal success or work as a team, as long as the larger agreed upon KPIs are aligned and being met."

> I always felt like OKRs really force every person to limit freedom to prioritize and limit flexibility. These ones fix that!

## Hiring as an objective

The complete hiring plan is kept in the Hiring Forecast doc. Hiring is not an objective in-and-of-itself. However hiring critical members for a team can be considered a key result. This is because recruiting top technical talent in a competitive startup environment can consume a large proportion of management's time and those hires are modeled into our product development goals. Keep all hiring-related KR's in the 'Team' objective.

## 2018-Q2

### CEO: Grow Incremental ACV according to plan. 120% of plan, pipeline 3x minus in quarter, 100% at 70% of quota.

  * CMO: Build 3x minus in quarter pipeline for Q3. % of plan achieved.
    * PMM: Complete messaging roll-out and activation to include: Sales, Partner and Marketing enablement, tier plans specific messaging and positioning, demo aligned to new positioning and messaging, presenting new messaging at key conferences.
    * PMM: Optimize trial sign-up, trial enablement and trial period experience, including the addition of gitlab.com trial and enhance trial nurture program.
 * CMO: Enough leads for sales. SCLAU of 10 per month for each SAL.
    * MSD: Scale sales development organization hiring to plan.
    * MSD: achieve volume target in inbound SCLAU generation.
    * MSD: achieve volume target in outbound SCLAU generation.
  * CRO: 120% of plan achieved.
  * CRO: Success Plans for all eligible customers.
  * CRO: Effective sales organization. 70% of salespeople are at 100% of quota.
    * Dir Channel: Increase Channel ASP by 50%
    * Dir Channel: Triple number of resellers above "Authorized Level"
    * Dir Channel: Implement VAR program (SHI, Insight, SoftwareOne, etc)
  * CFO
    * Legal: GDPR policy fully implemented.
    * Legal: Contract management system for non-sales related contracts.
    * Billing Specialist: Add cash collection, application and compensation to job responsibilities.

### CEO: Popular next generation product. Ship first iteration of complete DevOps, GitLab.com uptime, zero click cluster demo.

  * VP Product
    * Product: Ship first iteration of [complete DevOps](https://about.gitlab.com/2017/10/11/from-dev-to-devops/).
    * Product: Create a dashboard of usage of features. Replace Redash with Looker.
    * Product: Identify causes of free and paid churn on GitLab.com.
  * CFO: Make progress on having public clouds run us. 2 running everything.
    * Dir. Partnerships: Sign agreement to migrate target OS project
    * Dir. Partnerships: Strategic cloud partner chooses GitLab SCM for its single tenant offering
    * Dir. Partnerships: Successfully track and present data on the usage touch points for attribution tracking of our cloud agreement
  * CTO: Make sure cloud native installation, PaaS and cluster work well. Zero clicks.
  * CTO: Make sure we [use our own features](https://gitlab.com/gitlab-org/gitlab-ce/issues/43807). 3 features.
  * VPE
    * Eng Fellow: Improve monitoring by shipping 5 alerts that catch critical GitLab problems
    * UX: Deliver three UX Ready experience improvements per release towards reducing the installation time of DevOps.
    * UX: [Deliver three UX Ready experience improvements per release towards onboarding and authentication on gitlab.com.](https://gitlab.com/gitlab-org/ux-research/issues/54).
    * Quality: Deliver the first iteration of [engineering dashboard charts and metrics](https://gitlab.com/gitlab-org/gitlab-insights/issues/1).
    * Quality: Complete the organization of files, directories and LoC into /ee/ directory.
    * Security: Automated enforcement of GCP Security Guidelines
    * Security: Design, document, and implement security release process and craft epic with S1 & S2 issues and present to product for prioritization
    * Frontend: Deliver 100% of committed deliverables for each release (10.8: X%, 10.9: X%, 11.0: X%)
    * Frontend: Integrate the first 3 reusable Vue components based on design.gitlab.com
    * Dev Backend: Define KPIs and build monitoring for release cycle performance
    * Dev Backend: Create first iteration of engineering management training
    materials and merge into handbook
      * Platform: Deliver 100% of Deliverable issues for each release (10.8: X%, 10.9: X%, 11.0: X%)
      * Platform: Ship first GraphQL endpoint to be used by an existing frontend component
      * Discussion: Deliver 100% of Deliverable issues for each release (10.8: X% Deliverable / Y% total, 10.9: X% / Y%, 11.0: X% / Y%)
      * Discussion: Make GitLab a Rails 5 app by default
      * Distribution: Deliver 100% of committed issues for each release (10.8: X%, 10.9: X%, 11.0: X%)
      * Distribution: [Increase integration test coverage of HA setup](https://gitlab.com/gitlab-org/distribution/team-tasks/issues/124)
      * Geo: Deliver 100% of committed issues for each release (10.8: X%, 10.9: X%, 11.0: X%)
      * Geo: Test and perform multi-node secondary failover on GitLab.com to GCP
    * Ops Backend: Goal #1
    * Ops Backend: Goal #2
      * CI/CD: Deliver 100% of committed issues for each release (10.8: X%, 10.9: X%, 11.0: X%)
      * CI/CD: Cover demo of Auto DevOps with GitLab QA
      * Monitoring: Deliver 100% of committed issues for each release (10.8: X%, 10.9: X%, 11.0: X%)
      * Monitoring: Publish GitLab.com Grafana dashboards automatically
      * Security Products: [Gemnasium infrastructure moved to GitLab](https://gitlab.com/groups/gitlab-org/-/epics/66)
      * Security Products: [Gemnasium backend integrated into GitLab (Omnibus)](https://gitlab.com/groups/gitlab-org/-/epics/126)
    * Infrastructure: Maintain GitLab.com availability at 99.95%
      * Production: Move to GCP in April
      * Database: [Improve application performance](https://gitlab.com/gitlab-com/database/issues/44)
      * Database: [Improve monitoring, configuration, and knowledge of our infrastructure](https://gitlab.com/gitlab-com/database/issues/43<Paste>)
      * Database: [Improve team structure and workflow](https://gitlab.com/gitlab-com/infrastructure/issues/3917)
      * Gitaly: Get to v.1.1 remove rugged code
      * Gitaly: Test first iteration of GVFS implementation
    * Support Engineering: 100% SLA achievement for premium self-hosted customers
    * Support Engineering: Document and implement severity-based ticket processing workflow for self-hosted customers
      * Support Services: 100% SLA achievement for GitLab.com customers
      * Support Services: Develop and document Support Services workflows, processes, and automation needed to deliver world-class customer support
  * CMO: Generate more company and product awareness including increasing lead over [BitBucket in Google Trends](https://trends.google.com/trends/explore?q=bitbucket,gitlab).
    * MSD: Extend SEO/PPC/Digital Advertising programs to drive increase in number of free trials by 20% compared to last quarter, increase number of contact sales requests by 20% compared to last quarter, increase amount of traffic to about.gitlab.com by 10% compared to last quarter.
    * PMM: Plan and execute GTM for T1, Impreza and OSS announcements.
    * PMM: Submit strong submission for Gartner Application Release Orchestration (ARO) MQ and contribute to SCM Market Guide update and continue briefing sweep with all key Gartner and Forrester analysts.


### CEO: Great team. Active recruiting for all vacancies, number of diverse per vacancy, real-time dashboard.

  * CCO: Active recruiting. 100% of vacancies.
  * CCO: Increase double diverse candidates (underrepresented and low rent index). Number per vacancy triples QoQ.
  * CFO: Real-time dashboard for everything in the Metrics sheet. 100% of metrics.
    * Legal: Scalable solution for hiring added in at least 5 countries.
    * Controller: Close cycle reduced to 9 days.
    * Controller: Audit fieldwork completed with no material weaknesses reported.
  * VPE: Refactor engineering handbook to reflect org structure and have leaders take ownership of their sections
  * VPE: Source 150 candidates and hire a director of infra, director of support, and a prod manager
    * Dev Backend: Source 50 candidates and hire a geo manager
      * Discussion: Source 150 candidates and hire 3 developers
      * Platform: Source 150 candidates and hire 3 developers
      * Distribution: Source 50 candidates and hire 1 engineer
    * Ops Backend: Source 50 candidates and hire 1 monitoring manager
      * Monitoring: Source 100 candidates and hire 2 monitoring engineers
      * CI/CD: Source X candidates and hire X
      * Security Products: Source 50 candidates and hire 1 [Backend Developer](https://jobs.lever.co/gitlab/436d643c-1ff1-4115-8e4c-b1285ab72939)
    * Quality: Source 100 candidates and hire 2 test automation Engineers
    * Frontend: Source 150 candidates and hire 3 developers
    * Infrastructure: Source 50 candidates and hire a database manager
      * Production: Source 200 candidates and hire 4 production engineers
    * UX: Source 50 candidates and hire a UX designer
    * Security: Source 150 candidates and hire an Anti-abuse Analyst, a SecOps Engineer, and a Compliance Analyst
    * Support Engineering: Source 210 candidates and hire 1 support engineering manager and 6 support engineers
      * Support Services: Source X candidates and hire X
  * CMO: Hire to plan
    * CMO: Hire to DevRel team plan
    * CMO: Hire to Corporate Marketing team plan
    * PMM: Hire to PMM team plan
    * MSD: Hire to SDR team plan
    * MSD: Hire to Field Marketing team plan
    * MSD: Hire to Online Growth team plan

## Archive

* [2017-Q3](/okrs/2017-q3/)
* [2017-Q4](/okrs/2017-q4/)
* [2018-Q1](/okrs/2018-q1/)

[discussion-deliverables]: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=Deliverable
[discussion-sl1]: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=SL1
[discussion-sp1]: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=SP1
[discussion-ap1]: https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Discussion&label_name[]=backend&label_name[]=AP1
[discussion-backend-bug]: https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Discussion&label_name%5B%5D=backend&label_name%5B%5D=bug&scope=all&sort=updated_desc&state=opened
[discussion-backend-community-contributions]: https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Community%20Contribution&label_name[]=Discussion
[discussion-10-5-update]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/9934
[discussion-10-6-update]: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/10446
